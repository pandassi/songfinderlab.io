import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { Collapse, Button, CardBody, Card } from "reactstrap";
import "./style/song-finder.scss";
import ArticleCopyright from "./articles/article-copyright101";
import ArticleCreators from "./articles/article-creators";
import ArticleMemes from "./articles/article-memes";
import ArticleHowToCopyright from "./articles/article-how-to-copyright";
import ArticleCommonMyths from "./articles/common-myths";
import ArticlePlagiarism from "./articles/plagiarism";
import ArticleNotCovered from "./articles/not-covered";
import ArticleFiveCovers from "./articles/article-five-covers";
import ArticleEiffelTower from "./articles/eiffel-tower.js"
import CopyrightFashion from "./articles/copyright-fashion.js"

export default function App() {
	const [state, setState] = React.useState({
		articleOpen: "",
		moreInfoOpen: false,
	});
	const { article, moreInfoOpen } = state;

	// What is the current path
	useEffect(() => {
		// When const current path =
		const currentPath = window.location.pathname;
		// will hold
		if (currentPath === "/") {
			return;
		}

		const pathParts = currentPath.split("/");
		// Last part of the articleName
		const articleName = pathParts[pathParts.length - 1];
		toggleMoreInfo();

		if (articleName === "copyright-fashion") {
			handleArticleOpen("10");
		} else if (articleName === "eiffel-tower") {
			handleArticleOpen("9");
		} else if (articleName === "article-five-covers") {
			handleArticleOpen("8");
		} else if (articleName === "not-covered-by-copyright") {
			handleArticleOpen("7");
		} else if (articleName === "plagiarism") {
			handleArticleOpen("6");
		} else if (articleName === "common-myths") {
			handleArticleOpen("5");
		} else if (articleName === "article-how-to-copyright") {
			handleArticleOpen("4");
		} else if (articleName === "article-memes") {
			handleArticleOpen("3");
		} else if (articleName === "article-creators") {
			handleArticleOpen("2");
		} else if (articleName === "article-copyright101") {
			handleArticleOpen("1");
		}
	}, []);

	const toggleMoreInfo = () => {
		setState((prevState) => ({
			article: "",
			moreInfoOpen: !prevState.moreInfoOpen,
		}));
	};

	const handleArticleOpen = (article) => {
		setState((prevState) => ({
			...prevState,
			article,
		}));
	};

	return (
		<div>
			<Collapse isOpen={moreInfoOpen}>
				<Card className="card">
					<CardBody className="card-body">
						<div className="section section-articles">
							<div className="articles-buttons">
								{/* Router library designed which page is linked to url -> then display component
                                Here indexed? */}
								<Link to="/copyright-fashion">
									<Button
										className="copyright-fashion"
										color="primary"
										onClick={() => handleArticleOpen("10")}
									>
										<h3>Copyright in Fashion</h3>
									</Button>
								</Link>
								<Link to="/eiffel-tower">
									<Button
										className="eiffel-tower"
										color="primary"
										onClick={() => handleArticleOpen("9")}
									>
										<h3>Why You Can't Use Footage of the Eiffel Tower at Night</h3>
									</Button>
								</Link>

								<Link to="/five-covers">
									<Button
										className="article-five-covers"
										color="primary"
										onClick={() => handleArticleOpen("8")}
									>
										<h3>5 Global Music Hits That Are Actually Covers</h3>
									</Button>
								</Link>
								<Link to="/not-covered">
									<Button
										className="not-covered"
										color="primary"
										onClick={() => handleArticleOpen("7")}
									>
										<h3>What Is Not Covered By Copyright</h3>
									</Button>
								</Link>
								<Link to="/common-myths">
									<Button
										className="plagiarism"
										color="primary"
										onClick={() => handleArticleOpen("6")}
									>
										<h3>About Plagiarism</h3>
									</Button>
								</Link>
								<Link to="/common-myths">
									<Button
										className="myths"
										color="primary"
										onClick={() => handleArticleOpen("5")}
									>
										<h3>Common Copyright Myths Debunked</h3>
									</Button>
								</Link>
								<Link to="/article-how-to-copyright">
									<Button
										className="how-to-button"
										color="primary"
										onClick={() => handleArticleOpen("4")}
									>
										<h3>How To Copyright</h3>
									</Button>
								</Link>
								<Link to="/article-memes">
									<Button
										className="memes-button"
										color="primary"
										onClick={() => handleArticleOpen("3")}
									>
										<h3>Do Memes Infringe Copyright?</h3>
									</Button>
								</Link>
								<Link to="/article-creators">
									<Button
										className="creators-button"
										color="primary"
										onClick={() => handleArticleOpen("2")}
									>
										<h3>Copyright & Social Media</h3>
									</Button>
								</Link>
								<Link to="/article-copyright101">
									<Button
										className="copyright-button"
										color="primary"
										onClick={() => handleArticleOpen("1")}
									>
										<h3>Copyright 101</h3>
									</Button>
								</Link>
							</div>
							<Collapse isOpen={article === "9"}>
								<Card className="card">
									<CardBody className="card-body">
										<ArticleEiffelTower />
									</CardBody>
								</Card>
							</Collapse>
							<Collapse isOpen={article === "8"}>
								<Card className="card">
									<CardBody className="card-body">
										<ArticleFiveCovers />
									</CardBody>
								</Card>
							</Collapse>
							<Collapse isOpen={article === "7"}>
								<Card className="card">
									<CardBody className="card-body">
										<ArticleNotCovered />
									</CardBody>
								</Card>
							</Collapse>
							<Collapse isOpen={article === "6"}>
								<Card className="card">
									<CardBody className="card-body">
										<ArticlePlagiarism />
									</CardBody>
								</Card>
							</Collapse>
							<Collapse isOpen={article === "5"}>
								<Card className="card">
									<CardBody className="card-body">
										<ArticleCommonMyths />
									</CardBody>
								</Card>
							</Collapse>
							<Collapse isOpen={article === "4"}>
								<Card className="card">
									<CardBody className="card-body">
										<ArticleHowToCopyright />
									</CardBody>
								</Card>
							</Collapse>
							<Collapse isOpen={article === "3"}>
								<Card className="card">
									<CardBody className="card-body">
										<ArticleMemes />
									</CardBody>
								</Card>
							</Collapse>
							<Collapse isOpen={article === "2"}>
								<Card className="card">
									<CardBody className="card-body">
										<ArticleCreators />
									</CardBody>
								</Card>
							</Collapse>
							<Collapse isOpen={article === "1"}>
								<Card className="card">
									<CardBody className="card-body">
										<ArticleCopyright />
									</CardBody>
								</Card>
							</Collapse>
						</div>
					</CardBody>
				</Card>
			</Collapse>
		</div>
	);
}
